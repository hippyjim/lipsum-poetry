<?php

namespace LipsumPoetry;
use Google\Cloud\Translate\TranslateClient;
use Abraham\TwitterOAuth\TwitterOAuth;

class LipsumPoetry {

    public $lipsum;
    public $translation;
    public $size = 1;
    protected $config;
    protected $translateClient;

    public function __construct($config, $size = false)
    {
        $this->config = $config;
        if ($size) {
            $this->size = $size;
        }
        $this->translateClient = new TranslateClient(array('key'=> $this->config['translateClientKey']));
    }

    public function generateAndPost()
    {
        $this->getLipsum();
        $this->getTranslation();
        $this->postToTwitter();
    }

    public function getLipsum()
    {
        $lipsum = file_get_contents('https://loripsum.net/api/'.$this->size.'/short/plaintext');
        $this->lipsum = str_replace('Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '', $lipsum);
        $this->lipsum = str_replace('Duo Reges: constructio interrete. ', '', $this->lipsum);
        return $this->lipsum;
    }

    public function getTranslation()
    {
        $result = $this->translateClient->translate($this->lipsum, [
            'source' => 'la',
            'target' => 'en',
        ]);

        $this->translation = $result['text'];
        return $this->translation;
    }

    public function trimToTweetSize()
    {
        //Max size i 140 chars. We then truncate back to the last punctuation mark and add a full stop.
        $maxLengthString = substr($this->translation, 0, 140);

        $sentenceTrimmed = $this->sentenceTrim($maxLengthString, 140);

        if (!preg_match("/[.!?,;:]$/", $sentenceTrimmed)) {
            $sentenceTrimmed.='.';
        }
        return $sentenceTrimmed;
    }


    protected function sentenceTrim($string, $maxLength = 140) {
        $string = preg_replace('/\s+/', ' ', trim($string)); // Replace new lines (optional)

        if (mb_strlen($string) >= $maxLength) {
            $string = mb_substr($string, 0, $maxLength);

            $puncs  = array('. ', '! ', '? ', '; '); // Possible endings of sentence
            $maxPos = 0;

            foreach ($puncs as $punc) {
                $pos = mb_strrpos($string, $punc);

                if ($pos && $pos > $maxPos) {
                    $maxPos = $pos;
                }
            }

            if ($maxPos) {
                return mb_substr($string, 0, $maxPos + 1);
            }

            return rtrim($string) . '&hellip;';
        } else {
            return $string;
        }
    }


    public function postToTwitter()
    {
            $twitter = new TwitterOAuth(
                $this->config['consumer_key'],
                $this->config['consumer_secret'],
                $this->config['oauth_token'],
                $this->config['oauth_token_secret']
            );

            $status = $twitter->post(
                "statuses/update", [
                    "status" => $this->trimToTweetSize(),
                ]
            );
    }

}