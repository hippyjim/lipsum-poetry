<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title;?></title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Lipsum Poetry</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li <?php if(isset($currentPage) && $currentPage=='home') { echo 'class="active"';} ?> ><a href="/">Home</a></li>
                <li <?php if(isset($currentPage) && $currentPage=='about') { echo 'class="active"';} ?>><a href="/about">About</a></li>
            </ul>
        </div>
    </div>
</nav>
<div id="forkMe">
    <a href="https://bitbucket.org/hippyjim/lipsum-poetry" target="bb">
        <img src="/img/forkMeOnBitbucket.png" width="150" height="150" alt="Fork me on Bitbucket">
    </a>
</div>
<div class="container" role="main">