<?php

namespace LipsumPoetry;

class Template {

    public function header($title, $currentPage = 'home') {
        include('views/header.php');
    }

    public function footer() {
        include('views/footer.php');
    }


}