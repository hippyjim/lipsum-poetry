<?php

return [

    'consumer_key'      => 'TWITTER_CONSUMER_KEY',
    'consumer_secret'   => 'TWITTER_CONSUMER_SECRET',

    'url_login'         => 'http://URL/twitter_login.php',
    'url_callback'      => 'http://URL/twitter_callback.php',

    'oauth_token' => 'TWITTER_OAUTH_TOKEN',
    'oauth_token_secret' => 'TWITTER_OAUTH_TOKEN_SECRET',
    'translateClientKey' => 'GOOGLE_TRANSLATE_CLIENT_KEY',
];

