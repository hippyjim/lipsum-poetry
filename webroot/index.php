<?php

namespace LipsumPoetry;
require_once __DIR__.'/../vendor/autoload.php';

$config = require_once '../config.php';
$lipsumPoetry = new LipsumPoetry($config, 3);
$template = new Template();
$lipsum = $lipsumPoetry->getLipsum();
$result = $lipsumPoetry->getTranslation();

$template->header('Lipsum Poetry - creating beauty from filler text');
?>

<div class="col-md-9 col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Original Lipsum
                <span class="pull-right">via <a href="https://loripsum.net/">https://loripsum.net/</a></span>
            </h3>
        </div>
        <div class="panel-body">
            <?php echo $lipsum; ?>
        </div>
    </div>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Translated to English
                <span class="pull-right">via <a href="https://translate.google.com/">https://translate.google.com/</a></span>
            </h3>
        </div>
        <div class="panel-body">
            <?php echo $result; ?>
        </div>
    </div>
</div>
<div class="col-md-3 col-sm-12">
    <div class="panel panel-info">
        <div class="panel-body" style="padding: 2px;">
            <a class="twitter-timeline" data-tweet-limit="3" data-dnt="true" data-chrome="noborder transparent" data-theme="light" href="https://twitter.com/LipsumPoet">Tweets by LipsumPoet</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
    </div>

</div>

<?php

$template->footer();