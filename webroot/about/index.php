<?php

namespace LipsumPoetry;
require_once __DIR__.'/../../vendor/autoload.php';

use Michelf\Markdown;


$template = new Template();

$template->header('Lipsum Poetry - what is this about?', 'about');


?>
<div class="panel panel-info">
    <div class="panel-body">
    <?php echo Markdown::defaultTransform(file_get_contents('../../README.md')); ?>
    </div>
</div>

<?php

$template->footer();
