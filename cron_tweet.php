<?php

namespace LipsumPoetry;

require_once 'vendor/autoload.php';
$config = require_once 'config.php';

$lipsumPoetry = new LipsumPoetry($config);
$lipsumPoetry->generateAndPost();