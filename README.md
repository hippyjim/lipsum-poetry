What is this?
=============

This is an experiment in creating poetry by taking randomly generated Lorem Ipsum placeholder text, and translating it into English using Google translate.

Lorem Ipsum? Whuh?
==================

Lorem ipsum text has been used as "realistic" looking placeholder text since the 1960s - possibly earlier. It is said to have been originally generated from the works of Cicero, combined with other Latin texts. It is designed to be unreadable, so the viewer is not distracted from the layout by the content of the filler text. Lorem Ipsum (or Lipsum) was popularised by Letraset, who produced a set of Lorem Ipsum back in the 1960s. It was also used heavily by desktop publishing software such as Aldus Pagemaker in the 1980s.

Modern Lorem Ipsum generators use pseudo random strings, either taking snippets of the supposed original Cicero texts, or by using Bayesian algorithms to produce relaistic looking sentences based on Latin-like words.

Ok, so why do this?
===================

I work in web application design and frequently need to use Lipsum to test layouts. During my use of one of these generators, my browser offered to translate the page, and I accepted. The translated text was truly beautiful, if only semi-comprehensible. While some translations are total nonsense, every so often a translation produces a profound and insightful phrase. This project is my attempt to capture these for posterity, and to share this psuedo-random beauty with the world. It also makes interesting placeholder text in its own right.

So how does this work?
======================

This application takes Lorem Ipsum text generated via the API at [https://loripsum.net/](https://loripsum.net/) and feeds it through the Google Translate API, hopefully returning something vaguely comprehensible in English.

But why haven't you [insert idea here]?
=======================================

Other languages might follow, as might other options. If you have a specific feature request, feel free to contact me via Twitter - [@LipsumPoet](https://twitter.com/LipsumPoet)

Can I see the code?
===================

Sure. It's available at [https://bitbucket.org/hippyjim/lipsum-poetry](https://bitbucket.org/hippyjim/lipsum-poetry) - free to do with as you wish. It'd be cool if you let me know what you do with it though.